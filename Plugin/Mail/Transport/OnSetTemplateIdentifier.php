<?php

namespace Example\Logger\Plugin\Mail\Transport;

use Example\Logger\Model\Log;
use Magento\Framework\Mail\Template\TransportBuilder;


class OnSetTemplateIdentifier
{

    protected $log;

    public function __construct(
        Log $log
    )
    {
        $this->log = $log;
    }

    public function afterSetTemplateIdentifier(TransportBuilder $transportBuilder)
    {
        $logTemplateId = $this->getTemplateIdentiefer($transportBuilder);
        $this->log->setLogTemplateId($logTemplateId);

        return $transportBuilder;
    }

    private function getTemplateIdentiefer($transportBuilder)
    {
        $reflect = new \ReflectionClass($transportBuilder);
        $templateIdentifier = $reflect->getProperty('templateIdentifier');
        $templateIdentifier->setAccessible(true);

        return $templateIdentifier->getValue($transportBuilder);
    }

}