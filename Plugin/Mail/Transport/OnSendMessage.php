<?php

namespace Example\Logger\Plugin\Mail\Transport;


use Magento\Email\Model\Template\Config as EmailTemplateConfig;
use Example\Logger\Model\Log;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;


class OnSendMessage
{
    protected $log;

    public function __construct(
        Log $log,
        TransportInterface $transportInterface,
        DateTime $datetime,
        EmailTemplateConfig $emailTemplateConfig
    )
    {
        $this->log = $log;
        $this->transportInterface = $transportInterface;
        $this->datetime = $datetime;
        $this->emailTemplateConfig = $emailTemplateConfig;
    }

    /**
     * Wrap after \Magento\Framework\Mail\Transport -> sendMessage()
     */
    public function afterSendMessage(TransportInterface $subject)
    {
        try {
            $this->logSendMessageActivity($subject);
            return $subject;
        } catch (\Exception $e) {
            /*
             * error body  // var_dump($e->getMessage());die;
            */
            throw $e;
        }
    }

    /**
     * Logging logic
     * @param TransportInterface $subject
     */
    private function logSendMessageActivity(TransportInterface $subject)
    {

        $date = $this->datetime->gmtDate();
        $_message = $subject->getMessage();
        $type = $this->getTemplateType();
        $result = $this->log->log($_message, $date, $type);

        return $result;
    }

    private function getTemplateType()
    {

        $templateId = $this->log->getTemplateId();
        $moduleName = $this->emailTemplateConfig->getTemplateModule($templateId);
        $templateLabel = $this->emailTemplateConfig->getTemplateLabel($templateId);
        $templateType = $moduleName . '/' . $templateLabel;

        return $templateType;
    }
}