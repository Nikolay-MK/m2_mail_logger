<?php
namespace Example\Logger\Model\ResourceModel\Log;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'log_id';
    protected $_eventPrefix = 'example_logger_listing_collection';
    protected $_eventObject = 'listing_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Example\Logger\Model\Listing', 'Example\Logger\Model\ResourceModel\Listing');
    }

}