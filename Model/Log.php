<?php

namespace Example\Logger\Model;

class Log extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'example_logger';
    protected $_cacheTag = 'example_logger';
    protected $_eventPrefix = 'example_logger';
    protected $logTemplateIdentifier;
    protected $config;

    protected function _construct()
    {
        $this->_init('Example\Logger\Model\ResourceModel\Log');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }

    public function log($message, $date , $type)
    {
        $this->setType($type);
        $this->setEmailContent($message->getBody()->getRawContent());
        $this->setSendTime($date);
        $this->save();
    }

    public function setLogTemplateId($id)
    {
        $this->logTemplateIdentifier = $id;
    }

    public function getTemplateId()
    {
        return $this->logTemplateIdentifier;
    }



}